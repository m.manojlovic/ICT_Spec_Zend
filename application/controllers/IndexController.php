<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
        $this->view->headTitle()->prepend("Home");
    }

    public function indexAction()
    {
        $this->view->headTitle()->prepend("Index");
        $this->view->title = "Pocetna strana";
        $this->view->username = "Milos";
        $this->view->datum = date("d.M.Y");
        $this->view->content = "Ovo je sadrzaj pocetnog kontroler i index akcije";
        
        $menu_kategorije=array(
                    '<a href="#">Uncategorized</a> (3)',
                    '<a href="#">Lorem Ipsum</a> (42)',
                    '<a href="#">Urna Congue Rutrum</a> (28)',
                    '<a href="#">Vivamus Fermentum</a> (13)'
                );
        $lista = $this->view->htmlList($menu_kategorije, false, false, false);
        $this->view->placeholder("sidebar")->append("<h2>Kategorije</h2>" . $lista);
        
        $layout = $this->_helper->layout();
        $layout->test = "Ovo je test layout promenljive";
        
        // RestFull client call
        $restClient = new Zend_Rest_Client("http://vezba1.nawp.ict");
//        echo $restClient->restGet("/RestFull"); // indexAction by default
//        echo $restClient->restGet("/RestFull/1")->getRawBody();
//        echo $restClient->restPost("/RestFull")->getRawBody();
//        echo $restClient->restPut("/RestFull")->getRawBody();
//        echo $restClient->restDelete("/RestFull")->getRawBody();
        
        echo $restClient->restGet("/RestFull", array("id"=>4, "api_key"=>"1234"))->getRawBody();
        echo $restClient->restPost("/RestFull", array("id"=>5))->getRawBody();
        echo $restClient->restPut("/RestFull/id/6")->getRawBody();
        echo $restClient->restDelete("/RestFull/id/7")->getRawBody();
        
    }

    public function soapAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->getHelper("viewRenderer")->setNoRender(true);
        
        $server = new Zend_Soap_Server( null, array("uri"=>"http://vezba1.nawp.ict/index/soap") );
        $server->setClass("Bloger_Manager");
        $server->registerFaultException("Bloger_Exception");
        $server->handle();
    }

    public function wsdlAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->getHelper("viewRenderer")->setNoRender(true);
        
        $wsdl = new Zend_Soap_AutoDiscover();
        $wsdl->setClass("Bloger_Manager");
        $wsdl->setUri("http://vezba1.nawp.ict/index/soap");
        $wsdl->handle();
    }


}





