<?php

class LogovanjeController extends Zend_Controller_Action {

    public function init() {
        /* Initialize action controller here */
    }

    public function indexAction() {
        // action body
    }

    public function logoutAction() {
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        $sesija = new Zend_Auth_Storage_Session();
        $sesija->clear();
        
        $this->_redirect('/Index');
    }

}
