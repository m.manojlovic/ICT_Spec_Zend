<?php

class AdministracijaController extends Zend_Controller_Action
{

    protected $ulogovan = null;

    public function init()
    {
        $layout = $this->_helper->layout();


        $autentifikacija = Zend_Auth::getInstance();
        $this->ulogovan = $autentifikacija->hasIdentity();

        $korisnik = new Application_Model_Korisnik();
        $korisnik = $autentifikacija->getIdentity();

        if ($this->ulogovan != null) {
            $layout->logout = TRUE;
        }

//        print_r($korisnik->nazivUloga());
        if ($this->ulogovan && $korisnik->nazivUloga() != 'Administrator') {
            $this->_redirect('/Index');
        }

        if ($this->ulogovan == null) {
            $this->_redirect('/Formular/index');
        }

        /* Initialize action controller here */
        $menu_kategorije = array(
            $this->view->outputLink($this->view->url(array('controller' => 'Administracija', 'action' => 'slike'), 'default', true), 'Slike'),
            $this->view->outputLink($this->view->url(array('controller' => 'Administracija', 'action' => 'kategorije'), 'default', true), 'Kategorije'),
            $this->view->outputLink($this->view->url(array('controller' => 'Administracija', 'action' => 'korisnici'), 'default', true), 'Korisnici'),
            $this->view->outputLink($this->view->url(array('controller' => 'Administracija', 'action' => 'uloge'), 'default', true), 'Uloge'),
            $this->view->outputLink($this->view->url(array('controller' => 'Administracija', 'action' => 'post'), 'default', true), 'Postovi'),
            $this->view->outputLink($this->view->url(array('controller' => 'Administracija', 'action' => 'galerija'), 'default', true), 'Galerija'),
            $this->view->outputLink($this->view->url(array('controller' => 'Administracija', 'action' => 'test'), 'default', true), 'Test'),
        );
        $lista = $this->view->htmlList($menu_kategorije, false, false, false);
        $this->view->placeholder('sidebar')->append('<h2>Opcije</h2>' . $lista);
        
        $this->view->headScript()->prependFile('/js/jquery-1.11.2.min.js');
        $this->view->headScript()->prependFile('/js/test.js');
    }

    public function indexAction()
    {
        // action body
    }

    public function galerijaAction()
    {
        try {
            $request = $this->getRequest();
            $galerijaForm = new Application_Form_Galerija();
            $galerijaModel = new Application_Model_Galerija();

            $galerijaMapper = new Application_Model_GalerijaMapper();

            if ($request->getParam("operacija")) {
                $operacija = $request->getParam("operacija");
                $id = $request->getParam("id");

                if ($operacija == "izmena") {
                    if ($request->isPost() && $galerijaForm->isValid($_POST)) {
                        $galerijaModel->setId($id);
                        $galerijaModel->setNaziv($galerijaForm->getValue("tbNaziv"));

//                        $galerijaModel->update();
                        $galerijaMapper->save($galerijaModel);

                        $galerijaForm->reset();
                    } else {
                        $galerijaForm->setAction($galerijaForm->getAction() . "/operacija/" . $operacija . "/id/" . $id);

                        $tbNaziv = $galerijaForm->getElement("tbNaziv");

//                         $galerijaModel->setId($id);
//                         $tbNaziv->setValue( $galerijaModel->fetch()->naziv );

                        $galerijaMapper->find($id, $galerijaModel);
                        $tbNaziv->setValue($galerijaModel->getNaziv());

                        $btnSubmit = $galerijaForm->getElement("btnSubmit");
                        $btnSubmit->setLabel("Izmeni");
                    }
                }

                if ($operacija == "brisanje") {
                    $galerijaModel->setId($id);
                    $galerijaModel->delete();
                }

                if ($operacija == "unos" && $request->isPost() && $galerijaForm->isValid($_POST)) {
                    $galerijaModel->setNaziv($galerijaForm->getValue("tbNaziv"));
//                    $galerijaModel->insert();

                    $galerijaMapper->save($galerijaModel);

                    $galerijaForm->reset();
                }
            } else {
                $galerijaForm->setAction($galerijaForm->getAction() . "/operacija/unos");
            }

            $this->view->forma = $galerijaForm;

            $table_data = array();
//            foreach ($galerijaModel->fetchAll() as $row) {
            foreach ($galerijaMapper->fetchAll() as $row) {
                $admin_links = array(
//                    $this->view->outputLink($this->view->url(array('controller' => 'Administracija', 'action' => 'galerija', 'operacija' => 'izmena', 'id' => $row['idGalerija']), 'default', true), 'Izmeni'),
//                    $this->view->outputLink($this->view->url(array('controller' => 'Administracija', 'action' => 'galerija', 'operacija' => 'brisanje', 'id' => $row['idGalerija']), 'default', true), 'Obrisi')
                    $this->view->outputLink($this->view->url(array('controller' => 'Administracija', 'action' => 'galerija', 'operacija' => 'izmena', 'id' => $row->getId()), 'default', true), 'Izmeni'),
                    $this->view->outputLink($this->view->url(array('controller' => 'Administracija', 'action' => 'galerija', 'operacija' => 'brisanje', 'id' => $row->getId()), 'default', true), 'Obrisi')
                );

                $tmp = array();
                $tmp['id'] = $row->getId();
                $tmp['naziv'] = $row->getNaziv();
//                unset($row['idGalerija']);
                unset($tmp['id']);

//                $table_data[] = array_merge($admin_links, $row);
                $table_data[] = array_merge($admin_links, $tmp);
            }
            $this->view->tabela = $this->view->displayGenericTableHelper($table_data, array('', '', 'Naziv'), 1);
        } catch (Exception $ex) {
            $layout = $this->_helper->layout();
            $layout->message = "Izuzetak " . $ex->getMessage();
        }
    }

    public function slikeAction()
    {
        $slikaForma = new Application_Form_Slika();
        
        $this->view->forma = $slikaForma;
    }

    public function kategorijeAction()
    {
        // action body
    }

    public function postAction()
    {
        // action body
    }

    public function ulogeAction()
    {
        // action body
    }

    public function korisniciAction()
    {
        // action body
    }

    public function testAction()
    {
        try {
            $request = $this->getRequest();
            $galerijaForm = new Application_Form_Galerija();
            $testModel = new Application_Model_Test();
            $testMapper = new Application_Model_TestMapper();

//            $tmpTest = new Application_Model_Test();
//            $tmpTest->setText("Test 2");
//            $testMapper->save($tmpTest);

            if ($request->getParam("operacija")) {
                $operacija = $request->getParam("operacija");
                $id = $request->getParam("id");

                if ($operacija == "izmena") {
                    if ($request->isPost() && $galerijaForm->isValid($_POST)) {
                        $testModel->setId($id);
                        $testModel->setNaziv($galerijaForm->getValue("tbNaziv"));

//                        $galerijaModel->update();
                        $testMapper->save($testModel);

                        $galerijaForm->reset();
                    } else {
                        $galerijaForm->setAction($galerijaForm->getAction() . "/operacija/" . $operacija . "/id/" . $id);

                        $tbNaziv = $galerijaForm->getElement("tbNaziv");

//                         $galerijaModel->setId($id);
//                         $tbNaziv->setValue( $galerijaModel->fetch()->naziv );

                        $testMapper->find($id, $testModel);
                        $tbNaziv->setValue($testModel->getNaziv());

                        $btnSubmit = $galerijaForm->getElement("btnSubmit");
                        $btnSubmit->setLabel("Izmeni");
                    }
                }

                if ($operacija == "brisanje") {
                    $testModel->setId($id);
                    $testModel->delete();
//                    $testMapper->
                }

                if ($operacija == "unos" && $request->isPost() && $galerijaForm->isValid($_POST)) {
                    $testModel->setNaziv($galerijaForm->getValue("tbNaziv"));
//                    $galerijaModel->insert();

                    $testMapper->save($testModel);

                    $galerijaForm->reset();
                }
            } else {
                $galerijaForm->setAction($galerijaForm->getAction() . "/operacija/unos");
            }

            $this->view->forma = $galerijaForm;

            $table_data_test = array();
            foreach ($testMapper->fetchAll() as $row) {
                $admin_links = array(
//                    $this->view->outputLink($this->view->url(array('controller' => 'Administracija', 'action' => 'galerija', 'operacija' => 'izmena', 'id' => $row['idGalerija']), 'default', true), 'Izmeni'),
//                    $this->view->outputLink($this->view->url(array('controller' => 'Administracija', 'action' => 'galerija', 'operacija' => 'brisanje', 'id' => $row['idGalerija']), 'default', true), 'Obrisi')
                    $this->view->outputLink($this->view->url(array('controller' => 'Administracija', 'action' => 'test', 'operacija' => 'izmena', 'id' => $row->getId()), 'default', true), 'Izmeni'),
                    $this->view->outputLink($this->view->url(array('controller' => 'Administracija', 'action' => 'test', 'operacija' => 'brisanje', 'id' => $row->getId()), 'default', true), 'Obrisi')
                );

                $tmp = array();
                $tmp['id'] = $row->getId();
                $tmp['text'] = $row->getText();
//                unset($row['idGalerija']);
                unset($tmp['id']);

//                $table_data[] = array_merge($admin_links, $row);
                $table_data_test[] = array_merge($admin_links, $tmp);
            }
            $this->view->tabela = $this->view->displayGenericTableHelper($table_data_test, array('', '', 'Text'), 1);
        } catch (Exception $ex) {
            $layout = $this->_helper->layout();
            $layout->message = "Izuzetak " . $ex->getMessage();
        }
    }

//    public function ajaxTestAction()
//    {
//        $request = $this->getRequest();
//        if($request->isXmlHttpRequest()) {
//            $this->_helper->layout()->disableLayout();
//            $this->_helper->viewRenderer->setNoRender(true);
//            
//            $idGalerija = $request->getParam('idGalerija');
//            
//            $galerijaMapper = new Application_Model_GalerijaMapper();
//            $galerija = new Application_Model_Galerija();
//            
//            $galerijaMapper->find($idGalerija, $galerija);
//            
//            echo Zend_Json::encode($galerija, Zend_Json::TYPE_OBJECT); // trebaju nam public polja klase da bi mogao da ih konvertuje
//        }
//    }

    public function ajaxtestAction()
    {
        $request = $this->getRequest();
        if($request->isXmlHttpRequest()) {
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            
            $idGalerija = $request->getParam('idGalerija');
            
            $galerijaMapper = new Application_Model_GalerijaMapper();
            $galerija = new Application_Model_Galerija();
            
            $galerijaMapper->find($idGalerija, $galerija);
            
            echo Zend_Json::encode($galerija, Zend_Json::TYPE_OBJECT); // trebaju nam public polja klase da bi mogao da ih konvertuje
        }
    }


}




