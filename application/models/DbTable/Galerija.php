<?php

class Application_Model_DbTable_Galerija extends Zend_Db_Table_Abstract {

    protected $_name = 'galerija';
    protected $_id = 'idGalerija';
    protected $_dependentTables = array(
        'Application_Model_DbTable_Slika'
    );

}
