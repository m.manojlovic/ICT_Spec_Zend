<?php

class Application_Model_DbTable_Slika extends Zend_Db_Table_Abstract {

    protected $_name = 'slika';
    protected $_id = 'idSlika';
    protected $_referenceMap = array(
        'Galerija' => array( // naziv veze, moze biti proizvoljan
            'columns' => array('idGalerija'), // foreign key u tabeli slika
            'refTableClass' => 'Application_Model_DbTable_Galerija', // parent table
            'refColumns' => array('idGalerija') // parent table primary key
        )
    );
}
